from django.shortcuts import render, redirect
from .forms import PostForm
from .models import Form
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

class JadwalDelete(DeleteView):
        model = Form
        success_url = reverse_lazy('jadwal:jadwal')

def jadwal(request):
    post = Form.objects.all()
    post_form = PostForm(request.POST or None)


    if request.method == 'POST':
        # if 'id' in request.POST:
        #     Form.objects.get(id=request.POST['id']).delete()
        #     return redirect('jadwal:jadwal')
        
        if post_form.is_valid():
            post_form.save()
            return redirect('jadwal:jadwal')

    context = {
        'post_form' : post_form,
        'posting': post 
    }
    return render(request, 'jadwal.html',context)