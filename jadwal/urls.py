from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    url(r'(?P<pk>[0-9]+)/delete/$', views.JadwalDelete.as_view(), name='delete'),
]
