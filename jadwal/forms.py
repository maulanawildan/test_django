from django import forms
from .models import Form


class PostForm(forms.ModelForm):
    class Meta:
        model = Form
        fields = '__all__'
        widgets = {
            'tanggal' : forms.SelectDateWidget(),
            'jam' : forms.TimeInput(attrs={
                'class' : 'form-control',
                'type' : 'time'
            }),
        }